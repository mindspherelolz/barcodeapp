# barcodeapp

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# Mindsphere 雲端出貨系統 報告

## 一、應用場景
應用場景是出貨，也就是將倉儲貨品，依照出貨單上的指示，掃描出貨。整體流程大致可以切分為以下步驟：
1. 管理員派出貨單給現場人員。
2. 現場人員接收出貨單。
3. 現場人員取出要出貨的貨品。
4. 確認沒問題就出貨。
6. 紀錄。
![](https://i.imgur.com/Pk1ykyX.png)

目前以上流程是以人工方式完成，每個流程因缺乏系統性，不僅耗費人力，出錯可能性也很大，且不易追究。
1. 管理員如何發派出貨單給現場人員？口頭告知或依賴通訊軟體可能很方便，但並非長久之計。
2. 同上。
3. 這個環節是出錯的原因，但之後補救還來得及。
4. 如何確認沒有問題？依賴人工確認容易出錯。
5. 人工紙本紀錄，不僅記錄耗時，之後查找也不易。
![](https://i.imgur.com/cKkTkVu.png)

## 二、解決方案
此示範應用程式提供系統化的出貨解決方案。此解決方案中，加入了兩項工具：雲端、條碼掃描。

- 雲端：使用者可以在任何地方登入此出貨系統。雲端會自動記錄每一筆出貨資料，也能自動核銷判斷出貨是否正確。
- 條碼掃描：使用條碼認貨品，在出貨時可以直接用掃描機準確記錄貨品，防止人為的缺失。


![](https://i.imgur.com/b6k71uO.png)


解決方案使用流程：

1. 管理員新增出貨單
在新增出貨單的頁面中，管理員只要將欲出之貨品設定好，即完成新增，資料就會儲存至雲端資料庫。
2. 透過QR code認證
系統會給予「出貨單」以及「貨品」一個獨立的id，每個id都能轉成其對應的QR code，不論透過何種方式傳遞，系統都能透過QR code來認證每個出貨單與貨品。（註：在此示範中，為求簡略，系統會直接將條碼顯示於現場人員頁面。真實情況可能是條碼已黏貼在貨品上，而出貨單的條碼則透過公司通訊系統發送給現場人員，詳見〖其餘方案〗。）
3. 現場人員掃瞄出貨
在此示範中，現場人員必須先掃出貨單的條碼，方能進一步掃瞄貨品的條碼。每掃瞄貨品一次，資料就會立即上傳至雲端，網頁亦會即時顯示掃瞄結果。當現場人員認為已完成該出貨單後，就能提交。（註：在此示範中，直接將電腦視訊鏡頭作為掃描器，透過網頁前端直接將資料傳至後台系統。此掃描機有多種可行方案，詳見〖其餘方案〗。）
4. 系統判定及核銷
待出貨單被提交後，系統將會判定掃描結果是否正確，不論是否正確，皆會核銷該筆出貨單，並通知管理員。出貨結果會儲存到資料庫中。
![](https://i.imgur.com/SIV64xy.png)


## 三、架構簡介

1. MSP
Mindsphere平台，提供多種協定將資料傳輸至MSP資料庫、完整的API以取得資料、安全的權限界定的功能。使用者需要透過MSP來取得此示範應用。
2. 前端
使用Vue框架開發，利用Vue Router來實作單頁應用(Single Page Application)，賦予網頁應用流暢的操作性，並使用socket來達到即時反應。
3. 後端
使用express開發，實作Restful API與socket，連結前端使用者與資料庫。後端亦負責與MSP溝通。
4. 資料庫
使用cloud foundry MongoDB xs方案，負責儲存資料。


## 四、程式架構

### 1. 程式碼路徑配置
- config/：儲存app的配置，如mongoDB的路徑。
- model/：定義NoSQL documents的骨架。
- controller/：實作REST API。
    - 除了REST API以外，在itemList.js裡面實作了兩個api，一個用來透過網路上傳掃描資料（即利用電腦攝影機掃描代替掃描機），一個是用來提交核銷出貨單。
    - 裡面有個msp資料夾，是原先為了實作MSP API串接，後來沒有用到，可忽略。
- socket/：與REST API大同小異，為幾個需要即時回應的API實作event。
- middleware/
    - checkAdmin：僅讓管理員通過。
    - getUser：將使用者的資料存在req.user，方便之後使用。
    - getToken：取得token並儲存在req.token，方便之後使用。
    - getAdminToken：不需登入也可取得管理員身份。
- utils/：目前只使用到其中的routine.js。
- routes/：分化第一層路由。
- app.js：export app。
- db.js：export 初始化資料庫的function。
- server.js：執行app的入口。



### 2. 資料庫儲存項目
![](https://i.imgur.com/M6s5hUd.png)

- Item：儲存庫存貨品資料。
    - name：貨品的名字
    - category：貨品的種類
    - quantity：貨品的數量
- ItemList：儲存出貨單資料。
    - name：出貨單的名字
    - items：為一陣列，儲存項目為貨品的id與數量
    - scanList：對應到的ScanList id
    - production：該出貨單所屬的產線
    - operator：該出貨單指派給的現場人員（詳細功能尚未實作）
    - fulfilled：出貨單的項目使否已滿足，滿足的條件是：所有貨品都確實掃描正確數量，且無其他誤掃描之貨品。
    - submitted：是否已提交，提交後會核銷，並記錄至Record。
- ScanList：儲存現場人員針對某出貨單掃描的資料，在ItemList建立時，也同時建立ScanList。
    - itemList：對應到的ItemList
    - items：為一陣列，代表所需掃描的貨品，以及掃描進度
        - item：掃描貨品之id
        - demand：該貨品所需掃描的數量
        - scaned：該貨品已掃描的數量
        - wrong：記錄demand與scaned數量是否一致。
- Record：已完成的出貨單資料。
    - user：核銷出貨單的使用者（詳細功能尚未實作）
    - itemList：核銷的ItemList的id
    - scanList：核銷的ScanList的id
    - result：該筆核銷是否正確（與ScanList中fulfilled一致）
- BarcodeScanner：MSP資料庫轉換成app資料庫的橋樑（詳見〖細節解釋〗）
    - time：該筆資料上傳到MSP的時間
    - scannedValue：上傳到MSP的值
    - assetID：該筆資料屬於的asset，不同掃描器可能儲存到不同的asset
    - belong：該筆資料是屬於哪個ItemList
    - getLatestTime()：取得最近更新資料的時間
    - getLatestItemList()：取得最近更新資料所屬的ItemList
- User：儲存使用者資料
    - msp_user_name：使用者在MSP上的名字，通常是註冊email
    - email：使用者的email
    - department：使用者所屬部門
    - phone：使用者電話號碼
    - isAdmin：使用者是否為管理員

### 3. 細節解釋
#### ItemList與ScanList的關係
ItemList代表管理員所指派的出貨單，儲存「所有出貨的資訊」。在一個ItemList被建立時，會自動建立一個跟它相對應的ScanList。

ScanList在被建立時，就會依照ItemList的資料填入其所要的貨品。之後的每次掃描，app都會將掃描的資料存入對應的ScanList。

每次掃描會使ScanList更新，資料更新後會重新判斷該ScanList是否已被滿足，亦即所有貨品數量皆正確，且無其他誤掃之貨品。

#### BarcodeScanner與routine.js
由於app的資料庫和MSP的資料庫是分開的，因此使用者選擇將掃描資料傳給MSP資料庫（詳見〖其餘方案〗），app將無法接收到資料。因此必須使用routine.js以及BarcodeScanner來同步app和MSP的掃描資料。

實作原理如下：
1. 使用者掃描後，資料被存入MSP資料庫。
2. app在雲端執行時，每隔一段時間會執行routine.js。routine.js會透過MSP的timeseries API讀取資料庫中掃描的資料，並且以BarcodeScanner model存入app的資料庫。
3. 然而routine.js請求timeseries資料時，必須防止資料重複。因此routine.js在向MSP請求掃描資料時，會將最新上一筆資料的timestamp作為參數，如此一來，MSP回傳的資料就一定是最新的資料。

## 五、其餘方案
### 1. 掃描機

在此示範中，掃描機使用的是瀏覽器的攝影機，但這只是其中一種方法，還有直接傳至MSP的方式：
1. 現場佈置掃描機器，該機器連接PLC，並透過MindConnect Nano傳到MSP資料庫。後端再使用MSP API獲取掃瞄資料，存入資料庫。
2. 現場佈置掃瞄機器，該機器連接PC，並實作MindConnect Library傳到MSP資料庫。後端再使用MSP API獲取掃描資料，存入資料庫。
3. 現場佈置掃瞄機器，該機器連接PC，並使用MindConnect Edge傳到MSP資料庫。後端再使用MSP API獲取掃描資料，存入資料庫。

### 2. BarcodeScanner與routine.js
MSP API讓app能夠取得掃描的資料（如果掃描後是直接將資料傳給MSP），但這之間還需要解決兩個問題：一是使app和MSP裡的資料同步、二是如何正確的讓app解析MSP裡的資料。

前者目前的做法是每隔一段向MSP請求資料，並且利用「最新更新時間」來篩選重複的資料，但這個做法的缺點是，沒有立即性，且浪費雲端運算資源。同步最大的問題在於，app端無法得知掃描資料何時傳送到MSP資料庫，因此只要解決通知的問題，就能同步。MSP API有提供data flow engine的服務，能夠針對資料流做出回應，但要如何與app串接還需研究測試。

後者目前的做法是，使傳送至MSP的資料為一字串，且同時包含出貨單及貨品的ID，並以「:」分開，例如111223:667778，其中111223為出貨單ID，667778則是貨品ID。如此一來app取得該資料就能搜尋對應的出貨單，然後增加一筆掃描貨品的資料。當然這個方法有很多替代方案，例如使用者分別先掃描出貨單ID，再掃描貨品ID。方法很多，不一一贅述。


### 3. QR code
目前此示範尚無法取得廠商貨品ID，因此我們自動為新增的貨品生成，倘若未來若能與公司ERP系統整合（詳見〖未來拓展〗），就能直接使用公司提供的ID。此示範中，相同貨品的ID是也是相同的。

由於系統皆憑靠QR code認證貨品與出貨單，因此若條碼並未跟出貨單與貨品有直觀相連，反而更容易造成混淆。以下提供幾種方法使解決方案更完整：
1. 最直觀的想法是，在進貨的時候就一一為貨品貼上生成的QR code。如此一來，在出貨時，出了哪些貨，就會掃哪些條碼。
2. 管理人員在app新增出貨單後，需要一個系統性的方式通知現場人員。例如：生成出貨單pdf，用e-mail或是公司通訊系統告知現場人員。或是現場人員都在app線上待命，由app直接通知也可。



## 六、未來拓展
### App
雲端App目前實作的功能限於新增出貨單、掃描貨品、核銷出貨單，除了基本功能以外，還能添加功能讓使用者體驗更完善。以下提幾個例子：
- 統計儀表板
app目前僅顯示條列式的資料，若能另外提供圖表的方式呈現，管理員可以直觀地看出整體表現。
- 延伸帳號資料
目前資料庫已存有使用者的檔案，但沒有實質的功能，未來可以為現場人員的帳號新增項目，如績效指數、成功率等。

### 其他系統
此示範所觸及的範圍只有雲端，所有功能都用雲端完成。但實際使用情境還包括了現場端，以及公司本身的資料系統，未來要執行解決方案時，應該要依照真正的使用情境，將其餘資源納入藍圖之中。
- 使用本地端系統
於現場端部署掃描機與SCADA系統，所有掃描資料都在地端執行且判斷。SCADA需與雲端同步資料，接收新增的出貨單，並且回傳掃描後的資料。這樣的好處在於現場人員能夠操作即時反應的機台，不需要擔心網路延遲的問題，雲端變成記錄存檔的功能。
- 整合ERP
如果公司本身有ERP，且每個貨品有自己的ID，那麼QR code就能依此ID生成，掃描貨品時，除了通知雲端以外，也能串接ERP，直接減少ERP倉儲系統的存貨量。最終整合ERP、地端系統、雲端系統，使彼此環環相扣，資料一有變動，都能同步到其他系統上，如此應是此解決方案的終極目標。



