/* eslint-disable */
export default {
    data() {
        return {
            wrongList: []
        }
    },
    methods: {
        async getScanList() {
            try{
                let res = await this.$axios.get(`/backend/v1/scanlist/`);
                return res.data;
            }catch(e){
                console.log(e);
            }
        },
        async getScanListById(Id) {
            try{
                let res = await this.$axios.get(`/backend/v1/scanlist/${Id}`);
                return res.data;
            }catch(e){
                console.log(e);
            }
        },
        async getScanListByItemListId(Id) {
            try{
                let res = await this.$axios.get(`/backend/v1/scanlist/itemList/${Id}`);
                return res.data;
            }catch(e){
                console.log(e);
            }
        },
        async getWrongScanList(Id) {
            try{
                let res = await this.$axios.get(`/backend/v1/scanlist/wrong/${Id}`);
                this.wrongList = res.data;
                return res.data;
            }catch(e){
                console.log(e);
            }
        }
    }
}