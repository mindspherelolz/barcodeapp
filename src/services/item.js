/* eslint-disable */
import {setCookie,getCookie,delCookie} from '@/utilties/cookie';

export default {
    data() {
        return {
            // itemList: [],
            // tableData: [],
            item: {}
        }
    },
    computed: {
        itemList() {
            return this.$store.getters.itemList;
        },
        tableData() {
            return this.$store.getters.itemList;
        },
    },
    methods: {
        async getItem() {
            try{
                    let res = await this.$axios.get('/backend/v1/item');
                //     this.itemList = res.data;
                //     this.tableData =  this.itemList;
                //     if(this.value != ''){
                //         this.listFilter(this.value);
                //     }
                // return this.itemList;
                return res.data;
            }catch(e){
                console.log(e);
            }
        },
        async createItem() {
            try{
                let res = await this.$axios.post('/backend/v1/item',this.item);
                this.item = {};
                return res.data;
            }catch(e){
                alert('建立失敗');
                console.log(e);
                return false;
            }
        },
        async updateItem(Obj) {
            var target = {};
            target.name = Obj.name
            try{
                let res = await this.$axios.patch(`/backend/v1/item/${Obj._id}`,target);
                console.log(res.data);
                return res.data;
            }catch(e){
                alert('修改失敗');
                console.log(e);
                return false;
            }
        },
        async deleteItem(Id) {
            try{
                let res = await this.$axios.delete(`/backend/v1/item/${Id}`);
                return res.data;
            }catch(e){
                alert('刪除失敗');
                console.log(e);
                return false;
            }
        }
    }
}