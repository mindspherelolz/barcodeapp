/* eslint-disable */
export default {
    data() {
        return {
            // shipments: [],
            // unsubmittedShipments: [],
            // shipmentTable: [],
            selectedItem: {},
            itemsDetail: [],
            loading: false
        }
    },
    computed: {
        shipments() {
            return this.$store.getters.shipments;
        },
        shipmentTable() {
            return this.$store.getters.shipments;
        },
        unsubmittedShipments() {
            return this.$store.getters.unsubmittedShipments;
        },
    },
    methods: {
        async getShipment() {
            try{
                this.loading = true;
                let res = await this.$axios.get('/backend/v1/itemlist');
                // this.shipments = res.data;
                // this.shipmentTable =  this.shipments;
                // this.unsubmittedShipments = [];
                // this.shipments.forEach(shipment => {
                //     if(!shipment.submitted){
                //         this.unsubmittedShipments.push(shipment);
                //     }
                // });
                // return this.shipments;
                return res.data;
            }catch(e){
                console.log(e);
            } finally {
                this.loading = false;
            }
        },
        async createShipment(pendingArray,listName) {
            try{
                this.loading = true;
                var shipment = {
                    name: listName,
                    items: []
                }
                pendingArray.forEach(element => {
                    shipment.items.push({_id:element._id,quantity:element.quantity});
                });
                let res = await this.$axios.post('/backend/v1/itemlist',shipment);
                return res.data;
            }catch(e){
                alert('建立失敗');
                console.log(e);
                return false;
            }finally {
                await this.getShipment();
            }
        },
        async updateShipment(Obj) {
            var target = {};
            target.name = Obj.name
            try{
                this.loading = true;
                let res = await this.$axios.put(`/backend/v1/itemlist/${Obj._id}`,target);
                return res.data;
            }catch(e){
                alert('修改失敗');
                console.log(e);
                return false;
            }finally {
                await this.getShipment();
            }
        },
        async deleteShipment(Id) {
            try{
                this.loading = true;
                let res = await this.$axios.delete(`/backend/v1/itemlist/${Id}`);
                return res.data;
            }catch(e){
                alert('刪除失敗');
                console.log(e);
                return false;
            }finally {
                await this.getShipment();
            }
        },
        async getItemListDetails(shipmentId){
            try{
                this.loading = true;
                let res = await this.$axios.get(`/backend/v1/itemlist/${shipmentId}/items`);
                this.itemsDetail = res.data;
                return this.itemsDetail;
            }catch(e){
                console.log(e);
            }finally{
                this.loading = false;
            }
        },
        async scanItem(shipmentId,currentScan){
            try{
                this.loading = true;
                let res = await this.$axios.post(`/backend/v1/itemlist/scanItem/${shipmentId}`,currentScan);
                return res.data;
            }catch(e){
                console.log(e);
            }finally{
                this.loading = false;
            }
        },
        async submitItemList(shipmentId){
            try{
                this.loading = true;
                let res = await this.$axios.post(`/backend/v1/itemlist/submit/${shipmentId}`);
                return res.data;
            }catch(e){
                console.log(e);
            }finally{
                await this.getCurrent();
            }
        }
    }
}