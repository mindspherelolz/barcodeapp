/* eslint-disable */

export default {
    methods: {
        userUnSubscribe(){
            // Creation
            this.sockets.unsubscribe('USER_CREATED');
            // Deletion
            this.sockets.unsubscribe('USER_DELETED');
        },
        userCreateSubscribe(){
            this.sockets.subscribe('USER_CREATED', (data) => {
                this.getUser();
            });
        },
        userUpdateSubscribe(){
            this.sockets.subscribe('USER_UPDATED', (data) => {
                this.getUser();
            });
        },
        userDeleteSubscribe(){
            this.sockets.subscribe('USER_DELETED', (data) => {
                this.getUser();
            });
        },
        createUserSocket() {
                this.$socket.emit('CREATE_USER', this.user);
                this.user = {};
        },
        updateUserSocket(obj){
            this.$socket.emit('UPDATE_USER', obj);
        },
        deleteUserSocket(Id){
            this.$socket.emit('DELETE_USER', Id);
        }
    }
}