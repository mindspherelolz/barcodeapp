/* eslint-disable */

export default {
    methods: {
        itemUnSubscribe(){
            // Creation
            this.sockets.unsubscribe('itemCreated');
            // Deletion
            this.sockets.unsubscribe('itemDeleted');
        },
        itemCreateSubscribe(){
            this.sockets.subscribe('itemCreated', (data) => {
                // this.getItem();
                this.itemList.push(data);
                this.tableData =  this.itemList;
                if(this.value != ''){
                    this.listFilter(this.value);
                }
                // window.location.reload(true);
            });
        },
        itemDeleteSubscribe(){
            this.sockets.subscribe('itemDeleted', (data) => {
                // this.getItem();
                this.itemList = this.itemList.filter( (item) => {return item._id != data._id;});
                this.tableData =  this.itemList;
                if(this.value != ''){
                    this.listFilter(this.value);
                }
            });
        },
        createItemSocket() {
                this.$socket.emit('CREATE_ITEM', this.item);
                this.item = {};
        },
        deleteItemSocket(Id){
            this.$socket.emit('DELETE_ITEM', Id);
        },
        updateItemSocket(Obj){
            // var target = {};
            // target.name = Obj.name
            this.$socket.emit('UPDATE_ITEM', Obj);
        }
    }
}