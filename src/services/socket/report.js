/* eslint-disable */
export default {
    methods: {
        reportUnSubscribe(){
            // Creation
            this.sockets.unsubscribe('REPORT_CREATED');
            this.sockets.unsubscribe('ADMIN_REPORT_CREATED');
            this.sockets.unsubscribe('MAIN_REPORT_CREATED');
            this.sockets.unsubscribe('SITE_REPORT_CREATED');
            // Deletion
            this.sockets.unsubscribe('REPORT_DELETED');
            
        },
        reportCreateSubscribe(){
            this.sockets.subscribe('REPORT_CREATED', (data) => {
                this.getReport();
            });
        },
        adminReportCreateSubscribe(){
            this.sockets.subscribe('ADMIN_REPORT_CREATED', (data) => {
                this.admin.push(data);
            });
        },
        mainReportCreateSubscribe(){
            this.sockets.subscribe('MAIN_REPORT_CREATED', (data) => {
                this.getReport();
            });
        },
        siteReportCreateSubscribe(){
            this.sockets.subscribe('SITE_REPORT_CREATED', (data) => {
                this.getReport();
            });
        },
        reportUpdateSubscribe(){
            this.sockets.subscribe('REPORT_UPDATED', (data) => {
                this.getReport();
            });
        },
        reportDeleteSubscribe(){
            this.sockets.subscribe('REPORT_DELETED', (data) => {
                this.getReport();
            });
        },
        createReportSocket(report) {
            this.$socket.emit('CREATE_REPORT', report);
            // this.report = {};
        },
        updateReportSocket(obj){
            this.$socket.emit('UPDATE_REPORT', obj);
        },
        deleteReportSocket(Id){
            this.$socket.emit('DELETE_REPORT', Id);
        },
        checkReportSocket(Id){
            this.$socket.emit('CHECK_REPORT',Id);
        }
    }
}