/* eslint-disable */

export default {
    methods: {
        recordUnSubscribe(){
            // Deletion
            this.sockets.unsubscribe('recordDeleted');
        },
        recordDeleteSubscribe(){
            this.sockets.subscribe('recordDeleted', (data) => {
                this.getRecords();
            });
        },
        deleteRecordSocket(Id){
            this.$socket.emit('deleteRecord', Id);
        }
    }
}