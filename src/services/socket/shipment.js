/* eslint-disable */

export default {
    methods: {
        itemListUnSubscribe(){
            // Creation
            this.sockets.unsubscribe('ITEM_LIST_CREATED');
            // Deletion
            this.sockets.unsubscribe('DELETE_ITEM_LIST');
            // Scaned Item
            this.sockets.unsubscribe('ITEN_SCANNED');
            // Submitted Item List
            this.sockets.unsubscribe('ITEM_LIST_SUBMITTED');
        },
        itemListCreateSubscribe(){
            this.sockets.subscribe('ITEM_LIST_CREATED', (data) => {
                this.getShipment();
            });
        },
        itemListUpdateSubscribe(){
            this.sockets.subscribe('ITEM_LIST_UPDATED', (data) => {
                this.getShipment();
            });
        },
        itemListDeleteSubscribe(){
            this.sockets.subscribe('DELETE_ITEM_LIST', (data) => {
                this.getShipment();
            });
        },
        itemScanSubscribe(){
            this.sockets.subscribe('ITEM_SCANNED', (data) => {
                this.getCurrent();
            });
        },
        submitItemListSubscribe(){
            this.sockets.subscribe('ITEM_LIST_SUBMITTED', (data) => {
                this.shipmentId = '';
                this.shipmentName = '';
                this.getCurrent();
            });
        },
        createItemListSocket(pendingArray,listName) {
                var shipment = {
                    name: listName,
                    items: []
                }
                pendingArray.forEach(element => {
                    shipment.items.push({_id:element._id,quantity:element.quantity});
                });
                this.$socket.emit('CREATE_ITEM_LIST', shipment);
        },
        updateItemListSocket(obj){
            this.$socket.emit('UPDATE_ITEM_LIST', obj);
        },
        deleteItemListSocket(Id){
            this.$socket.emit('DELETE_ITEM_LIST', Id);
        },
        scanItemSocket(Id,target){
            this.$socket.emit('SCAN_ITEM', Id,target);
        },
        submitItemListSocket(Id,email){
            this.$socket.emit('SUBMIT_ITEM_LIST', Id,email);
            this.scannedShipment = '';
            this.currentPage++;
            this.getCurrent();
        }
    }
}