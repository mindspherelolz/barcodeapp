/* eslint-disable */
export default {
    methods: {
        checkPermission() {
            var user = this.$store.getters.user
            if(!user._id || !user.admin){
                this.$router.push('/');
                alert('權限不足!!');
            }
        }
    }
}