/* eslint-disable */
export default {
    data() {
        return {
            records: [],
            recordTable: [],
            loading: false
        }
    },
    methods: {
        async getRecords() {
            try{
                this.loading = true;
                let res = await this.$axios.get('/backend/v1/record');
                this.records = res.data;
                this.recordTable =  this.records;
                return this.records;
            }catch(e){
                console.log(e);
            } finally {
                this.loading = false;
            }
        },
        async deleteRecord(Id) {
            try{
                this.loading = true;
                let res = await this.$axios.delete(`/backend/v1/record/${Id}`);
                return res.data;
            }catch(e){
                alert('刪除失敗');
                console.log(e);
                return false;
            }finally {
                await this.getRecords();
            }
        }
    }
}