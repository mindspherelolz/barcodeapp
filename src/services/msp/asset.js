/* eslint-disable */
export default {
    data() {
        return {
            assets: [],
            currentAsset: {}
        }
    },
    methods: {
        async getAssets(){
            try{
                let res = await this.$axios.get('/backend/msp/assets');
                var data = res.data;
                return data;
            }catch(err){
                console.log(err)
            }
        }
    }
}