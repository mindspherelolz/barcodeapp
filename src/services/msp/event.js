/* eslint-disable */
export default {
    data(){
        return {
            events: [],
        }
    },
    methods: {
        async getEvents(value){
            var url = `/backend/msp/events/${value}`;
            try{
                let res = await this.$axios.get(url);
                var data = res.data;
                return data;
            }catch(err){
                console.log(err)
            }
        },
        async createEvent(event){
            var url = '/backend/msp/events/';
            try{
                let res = await this.$axios.post(url,event);
                var data = res.data;
                return data;
            }catch(err){
                console.log(err)
            }
        },
        async updateEvent(event){
            var url = `/backend/msp/events/${event.id}`;
            try{
                let res = await this.$axios.post(url,event);
                var data = res.data;
                return data;
            }catch(err){
                console.log(err)
            }
        },
        async deleteEvent(id){
            var url = `/backend/msp/events/deleteEventsJobs/${id}`;
            try{
                let res = await this.$axios.delete(url);
                return res.data;
            }catch(err){
                console.log(err)
            }finally{
                alert("Delete Successfully!");
                // this.events = await this.getEvents(this.currentAsset.assetId);
            }
        }
    }
}