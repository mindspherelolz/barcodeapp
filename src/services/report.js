/* eslint-disable */
export default {
    data() {
        return {
            // reportList: [],
            // tableData: [],
            // unAck: [],
            report: {},
            value: ''
        }
    },
    computed: {
        reportList() {
            return this.$store.getters.reports;
        },
        tableData() {
            return this.$store.getters.reports;;
        },
        unAck() {
            return this.$store.getters.unAck;
        },
    },
    methods: {
        listFilter(value) {
            this.tableData = this.itemList.filter(data => data.title === value);
        },
        async getReport() {
            try{
                let res = await this.$axios.get('/backend/v1/report');
                this.reportList = res.data;
                this.tableData =  this.reportList;
                if(this.value != ''){
                    this.listFilter(this.value);
                }
                this.unAck = this.reportList.filter((report) => { return !report.acknowledge});
                return this.reportList;
            }catch(e){
                console.log(e);
            }
        },
        async getMainReport() {
            try{
                let res = await this.$axios.get('/backend/v1/report/maintenance');
                this.maintenance = res.data;
                this.mainTableData =  this.maintenance;
                this.unAck = this.maintenance.filter((report) => { return !report.acknowledge});
                return this.maintenance;
            }catch(e){
                console.log(e);
            }
        },
        async getSiteReport() {
            try{
                let res = await this.$axios.get('/backend/v1/report/site');
                this.site = res.data;
                this.siteTableData =  this.site;
                this.unAck = this.site.filter((report) => { return !report.acknowledge});
                return this.site;
            }catch(e){
                console.log(e);
            }
        },
        async getAdminReport() {
            try{
                let res = await this.$axios.get('/backend/v1/report/admin');
                this.admin = res.data;
                this.adminTableData =  this.admin;
                this.unAck = this.admin.filter((report) => { return !report.acknowledge});
                return this.admin;
            }catch(e){
                console.log(e);
            }
        },
        async createReport() {
            try{
                let res = await this.$axios.post('/backend/v1/report',this.report);
                this.report = {};
                return res.data;
            }catch(e){
                alert('建立失敗');
                console.log(e);
                return false;
            }
        },
        async updateReport(Obj) {
            var target = {};
            target.name = Obj.name
            try{
                let res = await this.$axios.patch(`/backend/v1/report/${Obj._id}`,target);
                console.log(res.data);
                return res.data;
            }catch(e){
                alert('修改失敗');
                console.log(e);
                return false;
            }
        },
        async deleteReport(Id) {
            try{
                let res = await this.$axios.delete(`/backend/v1/report/${Id}`);
                return res.data;
            }catch(e){
                alert('刪除失敗');
                console.log(e);
                return false;
            }
        }
    }
}