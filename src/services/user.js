/* eslint-disable */
export default {
    data() {
        return {
            // userList: [],
            // tableData: [],
            user: {},
            userDetail: []
        }
    },
    computed: {
        userList() {
            return this.$store.getters.userList;
        },
        tableData() {
            return this.$store.getters.userList;
        },
    },
    methods: {
        async getUser() {
            try{
                let res = await this.$axios.get('/backend/v1/user/all');
                this.userList = res.data;
                this.userList.forEach(user => {
                    if((user.wrong.length + user.correct.length) > 0){
                        user.score = user.wrong.length / (user.correct.length + user.wrong.length);
                        user.score = 5 - (Math.round( ((user.score * 100) / 20) * 10) / 10);
                    } else{
                        user.score = 0;
                    }
                });
                this.tableData =  this.userList;
                if(this.value != ''){
                    this.listFilter(this.value);
                }
                return this.userList;
            }catch(e){
                console.log(e);
            }
        },
        async getUserById(Id) {
            try{
                let res = await this.$axios.get(`/backend/v1/user/id/${Id}`);
                this.userDetail = [];
                this.userDetail.push(res.data);
                console.log(this.userDetail[0]);
                return this.userDetail[0];
            }catch(e){
                console.log(e);
            }
        },
        async getUserByEmail(email) {
            try{
                let res = await this.$axios.get(`/backend/v1/user/email/${email}`);
                this.userDetail[0] = res.data;
                return this.userDetail[0];
            }catch(e){
                console.log(e);
            }
        },
        async createUser() {
            try{
                let res = await this.$axios.post('/backend/v1/user',this.user);
                this.user = {};
                return res.data;
            }catch(e){
                alert('建立失敗');
                console.log(e);
                return false;
            }
        },
        async updateItem(Obj) {
            var target = {};
            target.name = Obj.name
            try{
                let res = await this.$axios.patch(`/backend/v1/item/${Obj._id}`,target);
                console.log(res.data);
                return res.data;
            }catch(e){
                alert('修改失敗');
                console.log(e);
                return false;
            }
        },
        async deleteUser(Id) {
            try{
                let res = await this.$axios.delete(`/backend/v1/user/${Id}`);
                return res.data;
            }catch(e){
                alert('刪除失敗');
                console.log(e);
                return false;
            }
        }
    }
}