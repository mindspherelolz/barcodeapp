/* eslint-disable */
import Vue from 'vue';
import Router from 'vue-router';
import Navigator from '@/components/pages/Navigator'
//Admin
import Admin from '@/components/pages/administrator/Admin'
import Maintenance from '@/components/pages/administrator/Maintenance'
import ShipmentHandler from '@/components/pages/administrator/ShipmentHandler'
import ItemHandler from '@/components/pages/administrator/ItemHandler'
import UserHandler from '@/components/pages/administrator/UserHandler'
import ReportHandler from '@/components/pages/administrator/ReportHandler'
//Operator
import Operator from '@/components/pages/operator/Operator'
import Shipment from '@/components/pages/operator/Shipment'
import Report from '@/components/pages/operator/Report'

Vue.use(Router);

export default new Router ({
    // mode: 'history',
    routes: [
        {
            path:'/',
            name:'Navigator',
            component: Navigator
        },
        {
            path:'/admin',
            name:'Admin',
            component: Admin,
            children: [
                {
                    path: 'itemhandler',
                    name: 'ItemHandler',
                    component: ItemHandler
                },
                {
                    path: 'shipmenthandler',
                    name: 'ShipmentHandler',
                    component: ShipmentHandler
                },
                {
                    path: 'maintenance',
                    name: 'Maintenance',
                    component: Maintenance
                },
                {
                    path: 'userhandler',
                    name: 'UserHandler',
                    component: UserHandler
                },
                {
                    path: 'reporthandler',
                    name: 'ReportHandler',
                    component: ReportHandler
                }
              ]
        },
        {
            path:'/operator',
            name:'Operator',
            component: Operator,
            children: [
                {
                    path:'shipment',
                    name:'Shipment',
                    component: Shipment
                },
                {
                    path:'report',
                    name:'Report',
                    component: Report
                }
            ]
        }
    ]
});