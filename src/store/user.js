/* eslint-disable */
import axios from 'axios';

export default {
    state: {
        userStatus: 'admin',
        user: {},
        currentOperator: {}
      },
      mutations: {
        setUser(state,user) {
          state.userStatus = user;
        },
        setCurrentOperator(state,operator) {
          state.currentOperator = operator;
        },
        async setUserByToken(state){
          try{
            let res = await axios.get('/backend/msp/token/user');
            state.user = res.data;
            return state.user;
          }catch(e){
            console.log(e);
          }
        }
    },
    getters: {
        userStatus: state => state.userStatus,
        user: state => state.user,
        currentOperator: state => state.currentOperator
      }
    }