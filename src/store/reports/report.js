/* eslint-disable */
import Vue from 'vue'
import axios from 'axios';

export default {
    state: {
        reports: [],
        unAck: []
      },
      mutations: {
        async getReports(state){
          try{
            let res = await axios.get('/backend/v1/report');
            state.reports = res.data;
            state.unAck = state.reports.filter((report) => { return !report.acknowledge });
            return state.reports;
          }catch(e){
            console.log(e);
          }
        },
        SOCKET_REPORT_CREATED(state,report){
            state.reports.push(report);
        },
        SOCKET_REPORT_UPDATED(state,report){
            let index = state.reports.findIndex((data) => { return data._id == report._id});
            Vue.set(state.reports, index, report);
            state.unAck = state.reports.filter((report) => { return !report.acknowledge });
        },
        SOCKET_REPORT_DELETED(state,report){
            state.reports = state.reports.filter((element) => { return element._id != report._id });
            state.unAck = state.unAck.filter((element) => { return element._id != report._id });
        },
        SOCKET_REPORT_CHECKED(state,report){
            state.unAck = state.unAck.filter((element) => { return element._id != report._id });

        }
    },
    getters: {
        reports: state => state.reports,
        unAck: state => state.unAck
      }
    }