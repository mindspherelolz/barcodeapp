/* eslint-disable */
import Vue from 'vue'
import axios from 'axios';

export default {
    state: {
        adminReports: [],
        adminUnAck: []
      },
      mutations: {
        async getAdminReports(state){
          try{
            let res = await axios.get('/backend/v1/report/admin');
            state.adminReports = res.data;
            state.adminUnAck = state.adminReports.filter((report) => { return !report.acknowledge });
            return state.adminReports;
          }catch(e){
            console.log(e);
          }
        },
        SOCKET_REPORT_CHECKED(state,report){
          state.adminUnAck = state.adminUnAck.filter((element) => { return element._id != report._id });
        },
        SOCKET_REPORT_UPDATED(state,report){
          let index = state.adminReports.findIndex((data) => { return data._id == report._id});
          Vue.set(state.adminReports, index, report);
          state.adminUnAck = state.adminReports.filter((report) => { return !report.acknowledge });
        },
        SOCKET_REPORT_DELETED(state,report){
          state.adminReports = state.adminReports.filter((element) => { return element._id != report._id });
          state.adminUnAck = state.adminUnAck.filter((element) => { return element._id != report._id });
        },
    },
    getters: {
        adminReports: state => state.adminReports,
        adminUnAck: state => state.adminUnAck
      }
    }