/* eslint-disable */
import Vue from 'vue'
import axios from 'axios';

export default {
    state: {
        itemList: []
      },
      mutations: {
        async getItemList(state) {
            try{
                let res = await axios.get('/backend/v1/item');
                state.itemList = res.data;
                return state.itemList;
            }catch(e){
                console.log(e);
            }
        },
        SOCKET_ITEM_CREATED(state,item){
            state.itemList.push(item);
        },
        SOCKET_ITEM_UPDATED(state,item){
            let index = state.itemList.findIndex((data) => { return data._id == item._id});
            Vue.set(state.itemList, index, item);
        },
        SOCKET_ITEM_DELETED(state,item){
            state.itemList = state.itemList.filter((element) => { return element._id != item._id });
        }
    },
    getters: {
        itemList: state => state.itemList
      }
    }