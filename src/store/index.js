import Vue from 'vue';
import Vuex from 'vuex';

import user from './user';
//Users
import users from './users/user';
// Items
import items from './items/item';
// Shipments
import shipments from './shipments/shipment';
// Reports
import reports from './reports/report';
import adminReports from './reports/admin';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        user,
        items,
        shipments,
        adminReports,
        reports,
        users
    },
  });