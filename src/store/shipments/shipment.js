/* eslint-disable */
import Vue from 'vue'
import axios from 'axios';

export default {
    state: {
        shipments: [],
        unsubmittedShipments: [],
        currentShipment: {
            id: '',
            detail: [],
            wrongList: []
        }
      },
      mutations: {
        async getShipments(state) {
            try{
                let res = await axios.get('/backend/v1/itemlist');
                state.shipments = res.data;
                state.unsubmittedShipments = state.shipments.filter((shipment) => { return !shipment.submitted });
                return state.shipments;
            }catch(e){
                console.log(e);
            }
        },
        async shipmentScanned(state,id){
            try{
                state.currentShipment.id =id;
                // get current shipment detail 
                let detail = await axios.get(`/backend/v1/itemlist/${id}/items`);
                state.currentShipment.detail = detail.data;
                console.log(detail.data);
                // get wrong scanList detail
                let wrong = await axios.get(`/backend/v1/scanlist/wrong/${id}`);
                state.currentShipment.wrongList = wrong.data;
            }catch(e){
                console.log(e);
            }
        },
        SOCKET_ITEM_LIST_CREATED(state,shipment){
            state.shipments.push(shipment);
            state.unsubmittedShipments.push(shipment);
        },
        SOCKET_ITEM_LIST_UPDATED(state,shipment){
            let index = state.shipments.findIndex((data) => { return data._id == shipment._id});
            Vue.set(state.shipments, index, shipment);
            state.unsubmittedShipments = state.shipments.filter((shipment) => { return !shipment.submitted });
        },
        SOCKET_ITEM_LIST_DELETED(state,shipment){
            state.shipments = state.shipments.filter((element) => { return element._id != shipment._id });
            state.unsubmittedShipments = state.unsubmittedShipments.filter((element) => { return element._id != shipment._id });
        },
        SOCKET_ITEM_SCANNED(state,data){
            state.currentShipment.detail = data[0];
            state.currentShipment.wrongList = data[1];
        },
        SOCKET_ITEM_LIST_SUBMITTED(state,data){
            state.currentShipment = {
                id: '',
                detail: [],
                wrongList: []
            };
            state.unsubmittedShipments = state.unsubmittedShipments.filter((shipment) => { return shipment._id != data.itemList});
        }
    },
    getters: {
        shipments: state => state.shipments,
        unsubmittedShipments: state => state.unsubmittedShipments,
        currentShipment: state => state.currentShipment
      }
    }