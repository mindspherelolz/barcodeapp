/* eslint-disable */
import Vue from 'vue'
import axios from 'axios';

export default {
    state: {
        userList: []
      },
      mutations: {
        async getUsers(state) {
            try{
                let res = await axios.get('/backend/v1/user/all');
                state.userList = res.data;
                state.userList.forEach(user => {
                    if((user.wrong.length + user.correct.length) > 0){
                        user.score = user.wrong.length / (user.correct.length + user.wrong.length);
                        user.score = 5 - (Math.round( ((user.score * 100) / 20) * 10) / 10);
                    } else{
                        user.score = 0;
                    }
                });
                return state.userList;
            }catch(e){
                console.log(e);
            }
        },
        SOCKET_USER_CREATED(state,user){
            state.userList.push(user);
        },
        SOCKET_USER_UPDATED(state,user){
            let index = state.userList.findIndex((data) => { return data._id == user._id});
            Vue.set(state.userList, index, user);
        },
        SOCKET_USER_DELETED(state,user){
            state.userList = state.userList.filter((element) => { return element._id != user._id });
        }
    },
    getters: {
        userList: state => state.userList,
      }
    }